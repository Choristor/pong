﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartGame : MonoBehaviour {
	public float platformSpeed= 10.0f;
	public Vector3 whereToGo;
	public float speed= 1f;
	public int winPoint=5;
	public Transform player1;
	public Transform player2;
	public Transform player1ScoreText;
	public Transform player2ScoreText;

	int player1Points=0;
	int player2Points=0;
	// Use this for initialization
	void Start () {
		NewGame ();
	}
	private void NewGame(){
		whereToGo = new Vector3((float)Random.Range(25,75)/100.0f*(Random.Range(1,10)>5?-1:1),(float)Random.Range(25,75)/100.0f);
		transform.position = Vector3.zero;

	}
	private void GameReset(){
		NewGame ();
		player1Points = 0;
		player2Points = 0;
		player1ScoreText.GetComponent<Text>().text = "Score: 0";
		player2ScoreText.GetComponent<Text>().text = "Score: 0";
	}
	// Update is called once per frame
	void Update () {
		transform.position += whereToGo*Time.deltaTime*speed;
		Player1Movement ();
		Player2Movement ();

	}
	void FixedUpdate(){
		speed += 0.001f;
	}
	private void Player1Movement(){
		if (Input.GetKey (KeyCode.W)) {
			player1.position += Vector3.up * Time.deltaTime * platformSpeed;
		}
		if (Input.GetKey (KeyCode.S)) {
			player1.position += Vector3.down * Time.deltaTime * platformSpeed;
		}
	}
	private void Player2Movement(){
		if(Input.GetKey(KeyCode.UpArrow))
			player2.position += Vector3.up * Time.deltaTime * platformSpeed;
		if(Input.GetKey(KeyCode.DownArrow))
			player2.position += Vector3.down * Time.deltaTime * platformSpeed;
	}
	public void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.tag == "Player") {
			whereToGo = Vector3.Reflect (whereToGo,Vector3.right);
			speed += Time.deltaTime*5;
		}
		if (other.gameObject.tag=="Wall"  ) {
			whereToGo = Vector3.Reflect (whereToGo, Vector3.down);

		}
		if (other.gameObject.tag == "Score") {
			if (other.gameObject.name == "Player1Line") {
				player2Points++;
				player2ScoreText.GetComponent<Text>().text = "Score: " + player2Points;
			}
			if (other.gameObject.name == "Player2Line") {
				player1Points++;
				player1ScoreText.GetComponent<Text>().text = "Score: " + player1Points;
			}
			if (player1Points == winPoint) {
				Debug.Log ("Player1 Win");
				GameReset ();
			}
			if (player2Points == winPoint) {
				Debug.Log ("Player2 Win");
				GameReset ();
			}
			Debug.Log ("Player1: " + player1Points + " Player2: " + player2Points);
			NewGame ();

		}

	}

}
